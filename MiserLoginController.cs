using MISER.TranGen;
using MVCMiserLogin.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using MVCMiserLogin.CohesionWCF;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Security.Cryptography;
using ClienteWsSoap;
using System.Diagnostics;
using System.Data.Entity.Migrations;
using System.Configuration;

namespace MVCMiserLogin.Controllers
{
    public class UserQueryInfo
    {
        
        public string TransactionName { get; set; }//Ndhistiry or CFTIN
        public string TransactionJson { get; set; }//fields to send to web service
        public string UserId { get; set; }//Teller number
    
    }
    public class MiserLoginController : Controller

    {
        
        private static string _sessionToken;

        private const string SVC_NAMESPACE = "http://fisglobal.com/webservices";

        private static string _username = "username";
        private static string _password = "password";

        // GET: MiseLogin
        public ActionResult Index()
        {
            //string EmpNum = User.Identity.Name;
            //ViewBag.identityName = EmpNum;
            //TempData["EmpName"] = EmpNum;
            return View();
        }
        public ActionResult Error(string ErrMssg = "")
        {
            return View(model: ErrMssg);
        }
        public ActionResult LoggedIn()//method for the view
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            ITBOTDBEntities dc = new ITBOTDBEntities();
            int count = dc.sbuser_ncr_miser.Where(x => x.Teller_Num == EmpNum).Count();
            if (count <= 0)
                return RedirectToAction("Error", new { ErrMssg = "User not Loged In" });

            ViewBag.repsonse = "Not run";
            return View();
            // return PartialView("TransMonitorPartial");
        }

        [HttpPost]//send page details to the controller
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult LoggedIn
            (MiserLoginModel LoginDetails, /*string searchBySSN,*/ string SSN, 
            string mscur, string ndHis, string cfTin, string LogOut)//method for the view
        {
            
                string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");

                if (!string.IsNullOrEmpty(mscur))
                {
                    return RedirectToAction("MSCURviewPartial");
                }
                if (!string.IsNullOrEmpty(LogOut))
                {
                    return RedirectToAction("LogOut");
                }
            if (!string.IsNullOrEmpty(ndHis))// Commented
            {
                //List<Format00243> reply = NDHistory(EmpNum, "8306367");
                //List<NDHistoryModel> ndHistoryList = new List<NDHistoryModel>();
                //foreach (Format00243 record in reply)
                //{
                //    NDHistoryModel NdHistoryRecord = new NDHistoryModel();
                //    NdHistoryRecord.Balance = record.RR_SVHIS_ACCT_BAL.ToString();
                //    NdHistoryRecord.Date = record.RR_SVHIS_EFF_DT.ToString("MM-dd-yyyy");
                //    NdHistoryRecord.Time = record.RR_SVHIS_TIME.ToString();
                //    NdHistoryRecord.Amount = record.RR_SVHIS_AMT.ToString();
                //    NdHistoryRecord.Teller = record.RR_SVHIS_TEL_NBR.ToString();
                //    NdHistoryRecord.Branch = record.RR_SVHIS_BR.ToString();
                //    ndHistoryList.Add(NdHistoryRecord);
                //}

                //TempData["response"] = reply;
                //TempData.Keep();
                return RedirectToAction("ndHisPartial");
            }
            if (!string.IsNullOrEmpty(cfTin))
                {
                    List<Format00243> reply = CFTIN(EmpNum, "8306367");
                    string Balance = "";
                    string Date = "";
                    string Time = "";
                    string Amount = "";
                    string Teller = "";
                    foreach (Format00243 record in reply)
                    {
                        string balance = record.RR_SVHIS_ACCT_BAL.ToString();
                        string date = record.RR_SVHIS_EFF_DT.ToString("MM-dd-yyyy");
                        string time = record.RR_SVHIS_TIME.ToString();
                        string amount = record.RR_SVHIS_AMT.ToString();
                        string tellr = record.RR_SVHIS_TEL_NBR.ToString();
                        Balance += balance + "<br/>";
                        Date += date + "<br/>";
                        Time += time + "<br/>";
                        Amount += amount + "<br/>";
                        Teller += tellr + "<br/>";
                        // TempData["response"] = ndHisresponse;
                    }
                    TempData["Balance"] = Balance;
                    TempData["Date"] = Date;
                    TempData["Time"] = Time;
                    TempData["Amount"] = Amount;
                    TempData["Teller"] = Teller;
                    TempData.Keep();
                    return RedirectToAction("cfTinPartial");
                }
                //if (!string.IsNullOrEmpty(searchBySSN))
                //{

                //    //long ssnNumber = Convert.ToInt64(SSN);
                //    // return RedirectToAction("SearchBySSN_PartialView",new {SSN= ssnNumber });
                //    return RedirectToAction("Member360View");
                //}
                return PartialView("MonitorPartial");
                //return View();
            
        }
        
        public ActionResult TransMonitorPartial()
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            int EmpNumInt = Convert.ToInt32(EmpNum);
            ITBOTDBEntities dataContext = new ITBOTDBEntities();
            //IEnumerable<MonitorPatialModel> listofTrans = new IEnumerable<MonitorPatialModel>();
            //IEnumerable < MonitorPatialModel> listOfTrans =  dataContext.sbuser_TransactionLog.Where(x => x.TellerNumber == EmpNumInt);
            // DateTime CurrentDate = DateTime.Now.AddDays(-1);
            DateTime CurrentDate = DateTime.Now.Date;
            DateTime testRango = DateTime.Now.Date.AddDays(-15);
            IEnumerable<sbuser_TransactionLog> listOfTrans;
            listOfTrans = dataContext.sbuser_TransactionLog.Where
                (x => x.TellerNumber == EmpNumInt && x.CreationDate >= CurrentDate).OrderByDescending(x => x.CreationDate);
            //listOfTrans = dataContext.sbuser_TransactionLog.Where
            //    (x => x.TellerNumber == EmpNumInt && x.CreationDate > testRango).OrderByDescending(x => x.CreationDate);
            List<MonitorPartialModel> listOfTransactions = new List<MonitorPartialModel>();
            foreach (var item in listOfTrans)//copiar listOfTrans to listOfTransactions , both liest have the same details but are different type
            {

                var MonitorPartial = new MonitorPartialModel();
                MonitorPartial.BranchID = item.BranchID.ToString() ?? "";
                MonitorPartial.SourceAccountNumber = item.SourceAccountNumber.ToString() ?? "";
                MonitorPartial.DestinationAccountNumber = item.DestinationAccountNumber.ToString() ?? "";
                MonitorPartial.SourceAccountType = item.SourceAccountType.ToString() ?? "";
                MonitorPartial.DestinationAccountType = item.DestinationAccountType.ToString() ?? "";
                MonitorPartial.TransactionAmount = item.TransactionAmount.ToString() ?? "";
                MonitorPartial.Memo = item.Memo ?? "";
                MonitorPartial.SessionIdentifier = item.SessionIdentifier.ToString() ?? "";
                MonitorPartial.MachineGroupName = item.MachineGroupName ?? "";
                MonitorPartial.Status = item.Status.ToString() ?? "";
                MonitorPartial.TransactionIdentifier = item.TransactionIdentifier.ToString() ?? "";
                MonitorPartial.TransactionType = item.TransactionType.ToString() ?? "";
                MonitorPartial.CreationDate = item.CreationDate;
                listOfTransactions.Add(MonitorPartial);
            }

            TempData["EmpNum"] = EmpNum;
            TempData.Keep();
            //TempData["listOfTrans"]         
            ViewBag.response = "Not run";
            return PartialView(listOfTransactions);
        }
        
        [HttpPost]
        public ActionResult TestViewPartial(int selectedTab)
        {
            ViewBag.SelectedTab = selectedTab;
            return View();
        }
        public  ActionResult MonitorPartial()
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            int EmpNumInt = Convert.ToInt32(EmpNum);
            ITBOTDBEntities dataContext = new ITBOTDBEntities();
            //IEnumerable<MonitorPatialModel> listofTrans = new IEnumerable<MonitorPatialModel>();
            //IEnumerable < MonitorPatialModel> listOfTrans =  dataContext.sbuser_TransactionLog.Where(x => x.TellerNumber == EmpNumInt);
           // DateTime CurrentDate = DateTime.Now.AddDays(-1);
            DateTime CurrentDate = DateTime.Now.Date;
            IEnumerable<sbuser_TransactionLog> listOfTrans;
            //To show only current date
            //listOfTrans = dataContext.sbuser_TransactionLog.Where
            //    (x => x.TellerNumber == EmpNumInt && x.CreationDate >= CurrentDate).OrderByDescending(x => x.CreationDate);
            listOfTrans = dataContext.sbuser_TransactionLog.Where
                (x => x.TellerNumber == EmpNumInt).OrderByDescending(x => x.CreationDate);
            int? lastSessionID = listOfTrans.Select(x => x.SessionIdentifier).FirstOrDefault();
            listOfTrans = dataContext.sbuser_TransactionLog.Where
                (x => x.SessionIdentifier == lastSessionID);
            List<MonitorPartialModel> listOfTransactions = new List<MonitorPartialModel>();
               
            foreach (var item in listOfTrans)//copiar listOfTrans to listOfTransactions , both list have the same details but are different type
            {
              
                var MonitorPartial =new MonitorPartialModel();
                MonitorPartial.BranchID = item.BranchID.ToString() ?? "";
                MonitorPartial.SourceAccountNumber = item.SourceAccountNumber.ToString() ?? "";
                MonitorPartial.DestinationAccountNumber = item.DestinationAccountNumber.ToString() ?? "";
                MonitorPartial.SourceAccountType = item.SourceAccountType.ToString() ?? "";
                MonitorPartial.DestinationAccountType = item.DestinationAccountType.ToString() ?? "";
                MonitorPartial.TransactionAmount = item.TransactionAmount.ToString() ?? "";
                MonitorPartial.Memo = item.Memo ?? "";
                MonitorPartial.SessionIdentifier = item.SessionIdentifier.ToString() ?? "";
                MonitorPartial.MachineGroupName = item.MachineGroupName ?? "";
                MonitorPartial.Status = item.Status.ToString() ?? "";
                MonitorPartial.TransactionIdentifier = item.TransactionIdentifier.ToString() ?? "";
                MonitorPartial.TransactionType = item.TransactionType.ToString() ?? "";
                MonitorPartial.CreationDate = item.CreationDate;
                listOfTransactions.Add(MonitorPartial);
            }
            
            TempData["EmpNum"] = EmpNum;
            TempData.Keep();
            //TempData["listOfTrans"]         
            ViewBag.response = "Not run";
            listOfTransactions= listOfTransactions.OrderByDescending(x => x.CreationDate).ToList();
            return PartialView(listOfTransactions);
        }
        public ActionResult DownloadRcipt()
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            ITBOTDBEntities dc = new ITBOTDBEntities();
            DateTime? MaxDate = dc.sessions.Where(x=>x.UserName==EmpNum && x.SessionType == "PtmSession")
                .Max(x => x.StartTime);
            int? SessID = dc.sessions.Where(x=>x.StartTime == MaxDate && x.UserName == EmpNum && x.SessionType == "PtmSession")
                .FirstOrDefault().Identifier;
            List<sbuser_TransactionLog> TransactionsLogs = dc.sbuser_TransactionLog
                .Where(x=>SessID == x.SessionIdentifier && x.Status == 1).ToList();
            string text="";
            foreach (var trans in TransactionsLogs)
            {
                text += trans.SourceAccountNumber.ToString() + " " + trans.SourceAccountType+" "
                    +trans.DestinationAccountNumber+" "+ trans.DestinationAccountType+" "
                    +trans.TransactionType + " "+ trans.TransactionAmount+System.Environment.NewLine;   
                
            }
            string path = @"C:\Program Files (x86)\NCR\";
            //byte[] fileBytes = System.IO.File.ReadAllBytes(path + "NCRITreceipt.txt");
            //string fileName = "filename.txt";
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            
            
            return File(Encoding.UTF8.GetBytes(text), "text/plain", @"C:\Program Files (x86)\NCR\NCRITreceipt.txt");
        }
 
        public ActionResult MSCURviewPartial(MiserLoginModel LoginDetails)
        {
            if (TempData["response"] == null)
            {
                string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
                //ViewBag.identityName = EmpNum;
                sbuser_ncr_miser MiserDetails = new sbuser_ncr_miser();
                //MiserDetails.Miser_STA = LoginDetails.MiserStation;
                //MiserDetails.Teller_Num = LoginDetails.TellerNumber;
                ITBOTDBEntities dataContext = new ITBOTDBEntities();
                //sbuser_ncr_miser tellerDetails = dataContext.sbuser_ncr_miser.SingleOrDefault(x => x.Teller_Num == EmpNum);
                sbuser_ncr_miser tellerDetails = dataContext.sbuser_ncr_miser.FirstOrDefault(x => x.Teller_Num == EmpNum);
                //dataContext.sbuser_ncr_miser.Find(dataContext.sbuser_ncr_miser.Where(x => x.Teller_Num == EmpNum));
                if (tellerDetails != null)
                {
                    //tellerDetails.Miser_STA = LoginDetails.MiserStation;
                    LoginDetails.MiserStation = tellerDetails.Miser_STA;
                    LoginDetails.TellerNumber = tellerDetails.Teller_Num;
                    LoginDetails.Password = tellerDetails.Pin;
                    LoginDetails.CashBoxNum = tellerDetails.Cash_Box_Num;
                    LoginDetails.BegginingCash = tellerDetails.Beginning_Cash;
                    LoginDetails.DailyVarDate = tellerDetails.Daily_Var_Date;
                    string response = CallWebService(LoginDetails);
                    //string response = CallWebService(tellerDetails);
                    TempData["response"] = response;
                    TempData.Keep();
                }
                else
                {
                    TempData["response"] = "Not Found";
                    TempData.Keep();
                }
            }
            ViewBag.repsonse = "Not run";
            return PartialView();
        }

        [HttpPost]//send page details to the controller
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult MSCURviewPartial()
        {
            return PartialView();
        }

        public ActionResult TwoColmnPopUp(string input, string title = "", string ColName1 = "Name", string ColName2 = "SSN")
        {
            ViewBag.title = title;
            ViewBag.ColName1 = ColName1;
            ViewBag.ColName2 = ColName2;
            List<string> flagLst = input.Split(';').ToList();
            return PartialView(flagLst);
        }

        public ActionResult SearchByAccNumber(MiserLoginModel LoginDetails, string accNum2)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }

            List<Object> AllaccDetails = new List<object>();
            List<MiserReply> listMiserReply = new List<MiserReply>();
            List<long> accList = new List<long>();
            accList.Add(Convert.ToInt64(accNum2));
            string strdt = DateTime.Today.ToString();
            string enDt = DateTime.Now.ToString();
            if (string.IsNullOrWhiteSpace(accNum2))
                ViewBag.repsonse = "No data to display";
            else
            {
                listMiserReply = HistoryInquiry(accList, strdt, enDt, SessionToken);
            }
            if (listMiserReply.Count() <= 0)
            {
                return new HttpStatusCodeResult(400, "No Accounts found for " + accNum2);
                //throw new Exception("Member Not Found");
            }
            //List<Object> AllaccDetails = (from x in accDetails select (Object)x).ToList();
            //AllaccDetails.AddRange((from x in accDetails2 select (Object)x).ToList());
            var model = new NDHistoryModel()
            {
                // You'll likely want a .ToList() after these to ensure things work as expected
                MiserReply = listMiserReply,
                AllaccDetails = AllaccDetails,
            };
            //return PartialView(listMiserReply);
            SessionDisconnect("", SessionToken);
            SessionToken = "";
            return PartialView(model);
            // return PartialView(AllaccDetails);
        }
        public ActionResult SearchByAddress(string Address)
        {

            #region sessionToken
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            ITBOTDBEntities dc = new ITBOTDBEntities();
            sbuser_ncr_miser login = dc.sbuser_ncr_miser.Where(x => x.Teller_Num.ToString() == EmpNum)
            .FirstOrDefault();
            MiserStation sta = new MiserStation
            {
                StationName = login.Miser_STA,
                TellerNumber = login.Teller_Num,
                Pin = login.Pin,
                CashBoxNumber = login.Cash_Box_Num,
                //BeginningCash = login.Cash_Box_Num,
                //DailyVariableDate = login.Cash_Box_Num
            };
            LogonResult logonResult;
            string SessionToken = "";
            using (CohesionConnectClient ccc = new CohesionConnectClient())
            {
                logonResult = ccc.SessionCreate(sta);
                SessionToken = logonResult.Token;
                _sessionToken = SessionToken;
                if (string.IsNullOrWhiteSpace(logonResult.Token))
                    Debug.WriteLine("Token: " + logonResult.Message);
            }
            #endregion

            Address = Address.Replace(".", "");
            Address = Address.Length <= 15 ? Address : Address.Substring(0, 15);
            CFDRI cfdri = new CFDRI();
            cfdri.ICF_MM_CUST_TYPE = "P";
            cfdri.ICF_MM_ADDR_KEY = Address;
            cfdri.ICF_MM_ADDR_KEY_IND = 1;
            cfdri.ICF_DRI_REQ_1 = 124;

            string xml = cfdri.ToMiser();

            MiserReply miserReply = new MiserReply(GetSessionHostReply(xml, _sessionToken));
            if (miserReply.Errors.Count() > 0
            && (miserReply.Errors.Contains("ERR000 NO DATA ENTERED")
            || (miserReply.Errors.Contains("ERR000 CIF NOT FOUND"))))
            {
                return new HttpStatusCodeResult(400, "Member Not Found " + Address);
            }
            if (miserReply.Errors.Count() > 0
                 && (miserReply.Errors.Contains("ERR000 EMPLOYEE PIN CHG REQUIRED")))
            {
                return new HttpStatusCodeResult(400, "PIN change required to continue.");
            }
            if (miserReply.Errors.Count() > 0
                && (miserReply.Errors.Contains("Invalid session identifier used. Cannot process transaction.")))
            {
                return new HttpStatusCodeResult(400, "Invalid session identifier used.");
            }
            if (miserReply.Errors.Count() > 0
                 && (miserReply.Errors.Contains("ERR000 ADDRESS NOT FOUND")))
            {
                return new HttpStatusCodeResult(400, "ADDRESS NOT FOUND.");
            }
            if (miserReply.Errors.Count() > 0)
            {
                throw new Exception(string.Join(" / ", miserReply.Errors));
            }

            if (miserReply.Status != "A")
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            else
            {
                ViewBag.repsonse = "Success Results";
            }

            SessionDisconnect("", SessionToken);
            SessionToken = "";

            List<MemberAddress> memberAddresses = new List<MemberAddress>();

            foreach (var f119 in miserReply.Formats.OfType<Format00119>())
            {
                MemberAddress ma = new MemberAddress();

                ma.Name = f119.RR_CFDUP_NA_LINE_1;
                ma.StAddress = f119.RR_CFDUP_NA_LINE_2;
                ma.CSZAddress = f119.RR_CFDUP_NA_LINE_3;
                ma.SSN = f119.RR_CFDUP_TAX_ID_NBR.ToString().PadLeft(9, '0');
                ma.CIF = f119.RR_CFDUP_CIF_NBR.ToString();

                memberAddresses.Add(ma);
            }

            foreach (var f112 in miserReply.Formats.OfType<Format00112>())
            {
                MemberAddress ma = new MemberAddress();

                ma.Name = f112.RR_CFCMI_NA_LINE_1;
                ma.StAddress = f112.RR_CFCMI_NA_LINE_2;
                ma.CSZAddress = f112.RR_CFCMI_NA_LINE_3;
                ma.SSN = f112.RR_CFCMI_TAX_ID_NBR.ToString().PadLeft(9, '0');
                ma.CIF = f112.RR_CFCMI_CIF_NBR.ToString();

                memberAddresses.Add(ma);
            }

            return PartialView(memberAddresses);

        }
        public ActionResult ndHisPartial(MiserLoginModel LoginDetails, string accNum, string strDT, string enDt)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            List<long> accList = new List<long>();
            List<Object> AllaccDetails = new List<object>();
            List<MiserReply> ListaccDets = new List<MiserReply>();
            List<MiserReply> listMiserReply = new List<MiserReply>();
            string MaturityDate = "", appCode="", balance="";

            accList.Add(Convert.ToInt64(accNum));

            if (string.IsNullOrWhiteSpace(accNum))
                ViewBag.repsonse = "No data to display";
            else
            {
                listMiserReply = HistoryInquiry(accList, strDT, enDt, SessionToken);
               
                foreach (var item in listMiserReply)
                {
                    //var modelo = new NDHistoryModel();
                    string ssn = item.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_TAX_ID_1.ToString().PadLeft(9, '0'); ;
                     appCode = item.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_APPL_CODE.ToString();
                    //modelo.Balance = item.Formats.OfType<MISER.TranGen.Format00243>().FirstOrDefault().RR_SVHIS_ACCT_BAL.ToString();
                    // works only for CD, SV , and ND. Loand do not have bal, they have principal
                    //balance = item.Formats.OfType<MISER.TranGen.Format00243>().FirstOrDefault().RR_SVHIS_ACCT_BAL.ToString();
                    if (appCode == "CD")
                    {
                        MiserReply miserReplyCFDRI = MiserReply_CFDRI(ssn.ToString(), SessionToken);
                            foreach (MISER.TranGen.Format00116 cfdritem in miserReplyCFDRI.Formats.OfType<MISER.TranGen.Format00116>()
                            .Where((x => x.RR_CFCAR_ACCT_NBR.ToString().PadLeft(12, '0').Equals(accNum.ToString().PadLeft(12, '0')))))
                        {
                            string cfdriAccNum = cfdritem.RR_CFCAR_ACCT_NBR.ToString();
                             MaturityDate = cfdritem.RR_CFCAR_MAT_DUE_DT.ToString();
                            //ListaccDets = ListaccDets.Add(cfdritem);
                        }
                    }
                }
            }

            var model = new NDHistoryModel()
            {
                // You'll likely want a .ToList() after these to ensure things work as expected
                MiserReply = listMiserReply,
                AllaccDetails = AllaccDetails,
                accDets = MaturityDate,
                AppCode = appCode,
                Balance = balance,
            };
            //return PartialView(listMiserReply);
            SessionDisconnect("", SessionToken);
            SessionToken = "";
            ViewBag.startDate = strDT;
            return PartialView(model);
           // return PartialView(AllaccDetails);
        }
        [HttpPost]//send page details to the controller
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult ndHisPartial()
        {
            return PartialView();
        }
        public string getToken()
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            ITBOTDBEntities dc = new ITBOTDBEntities();
            sbuser_ncr_miser login = dc.sbuser_ncr_miser.Where(x => x.Teller_Num.ToString() == EmpNum)
                .FirstOrDefault();
            MiserStation sta = new MiserStation
            {
                StationName = login.Miser_STA,
                TellerNumber = login.Teller_Num,
                Pin = login.Pin,
                CashBoxNumber = login.Cash_Box_Num,
                //BeginningCash = login.Cash_Box_Num,
                //DailyVariableDate = login.Cash_Box_Num
            };

            LogonResult logonResult;
            string SessionToken = "";
            using (CohesionConnectClient ccc = new CohesionConnectClient())
            {
                logonResult = ccc.SessionCreate(sta);
                SessionToken = logonResult.Token;
                _sessionToken = SessionToken;
                if (string.IsNullOrWhiteSpace(logonResult.Token))
                    Debug.WriteLine("Token: " + logonResult.Message);
            }
            return SessionToken;
        }
        public MiserReply MiserReply_CFDRI(string SSN , string SessionToken)
        {
            // NDandSVdetails ndAndSVdetails = new NDandSVdetails();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            //    CFTIN cFTIN = new CFTIN();
            // //Convert.ToInt32(SSN);
            long LongSSN = long.Parse(SSN);
            MiserReply miserReplyCFDRI=null;
            CFDRI cFDRI = new CFDRI();
            if (SSN.Length == 9)
            { //Search by SSN
                
                cFDRI.ICF_MM_TAX_ID = LongSSN;
                // cFDRI.ICF_CIF_NBR = LongSSN;
                cFDRI.ICF_MM_CUST_TYPE = "P";
                cFDRI.ICF_DRI_REQ_1 = 124;
                string xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A" || miserReplyCFDRI.Formats.OfType<Format00119>().Count() > 0)
                {
                    // Search by CIF
                    string SSNreset = "0";
                    long SSNresetLong = long.Parse(SSNreset);
                    cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                    cFDRI.ICF_CIF_NBR = LongSSN;
                    cFDRI.ICF_MM_CUST_TYPE = "P";
                    cFDRI.ICF_DRI_REQ_1 = 124;
                    xmlCFDRI = cFDRI.ToMiser();
                    miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                    if (miserReplyCFDRI.Status != "A")
                    {
                        ViewBag.repsonse = "Unsuccessful result";
                    }
                    else
                    {
                        ViewBag.repsonse = "Success Results";
                    }
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            else
            {
                // Search by CIF
                string SSNreset = "0";
                long SSNresetLong = long.Parse(SSNreset);
                cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                cFDRI.ICF_CIF_NBR = LongSSN;
                cFDRI.ICF_MM_CUST_TYPE = "P";
                cFDRI.ICF_DRI_REQ_1 = 124;
                string xmlCFDRI = cFDRI.ToMiser();
                xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            
            //SessionDisconnect("", _sessionToken);
            //_sessionToken = "";
            return miserReplyCFDRI;
        }
        public MiserReply MiserReply_CFDRI_ATM(string SSN, string appCode, long accNumber, string SessionToken)
        {
            //string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            long LongSSN = long.Parse(SSN);

            MiserReply miserReplyCFDRI = null;
            CFDRI cFDRI = new CFDRI();
            if (SSN.Length == 9)
            { //Search by SSN
                cFDRI.ICF_MM_TAX_ID = LongSSN;
                // cFDRI.ICF_CIF_NBR = LongSSN;
                cFDRI.ICF_MM_CUST_TYPE = "P";
                cFDRI.ICF_DRI_REQ_1 = 124;
                string xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A" || miserReplyCFDRI.Formats.OfType<Format00119>().Count() > 0)
                {
                    // Search by CIF
                    string SSNreset = "0";
                    long SSNresetLong = long.Parse(SSNreset);
                    cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                    cFDRI.ICF_CIF_NBR = LongSSN;
                    cFDRI.ICF_MM_CUST_TYPE = "P";
                    cFDRI.ICF_DRI_REQ_1 = 124;
                    xmlCFDRI = cFDRI.ToMiser();
                    miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                    if (miserReplyCFDRI.Status != "A")
                    {
                        ViewBag.repsonse = "Unsuccessful result";
                    }
                    else
                    {
                        ViewBag.repsonse = "Success Results";
                    }
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            else
            {
                // Search by CIF
                string SSNreset = "0";
                long SSNresetLong = long.Parse(SSNreset);
                cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                cFDRI.ICF_CIF_NBR = LongSSN;
                cFDRI.ICF_MM_CUST_TYPE = "P";
                cFDRI.ICF_DRI_REQ_1 = 124;
                string xmlCFDRI = cFDRI.ToMiser();
                xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            //SessionDisconnect("", _sessionToken);
            //_sessionToken = "";
            return miserReplyCFDRI;
        }
        public MiserReply MiserReply_CFDRI_Flag(string SSN, string appCode, long accNumber, string SessionToken)
        {
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            long LongSSN = long.Parse(SSN);

            MiserReply miserReplyCFDRI = null;
            CFDRI cFDRI = new CFDRI();
            if (SSN.Length == 9)
            {
                cFDRI.ICF_MM_TAX_ID = LongSSN;
                cFDRI.ICF_MM_APPL_CODE = appCode;
                cFDRI.ICF_ACCT_NBR = accNumber;
                cFDRI.ICF_DRI_REQ_1 = 123;
                string xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A" || miserReplyCFDRI.Formats.OfType<Format00119>().Count() > 0)
                {
                    // Search by CIF
                    string SSNreset = "0";
                    long SSNresetLong = long.Parse(SSNreset);
                    cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                    cFDRI.ICF_CIF_NBR = LongSSN;
                    cFDRI.ICF_MM_APPL_CODE = appCode;
                    cFDRI.ICF_ACCT_NBR = accNumber;
                    cFDRI.ICF_DRI_REQ_1 = 123;
                    xmlCFDRI = cFDRI.ToMiser();
                    miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                    if (miserReplyCFDRI.Status != "A")
                    {
                        ViewBag.repsonse = "Unsuccessful result";
                    }
                    else
                    {
                        ViewBag.repsonse = "Success Results";
                    }
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            else
            {
                // Search by CIF
                string SSNreset = "0";
                long SSNresetLong = long.Parse(SSNreset);
                cFDRI.ICF_MM_TAX_ID = SSNresetLong;//reset SSN
                cFDRI.ICF_CIF_NBR = LongSSN;
                cFDRI.ICF_MM_APPL_CODE = appCode;
                cFDRI.ICF_ACCT_NBR = accNumber;
                cFDRI.ICF_DRI_REQ_1 = 123;
                string xmlCFDRI = cFDRI.ToMiser();
                xmlCFDRI = cFDRI.ToMiser();
                miserReplyCFDRI = new MiserReply(GetSessionHostReply(xmlCFDRI, _sessionToken));
                if (miserReplyCFDRI.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    ViewBag.repsonse = "Success Results";
                }
            }
            
            //Search ticklers for warning accounts
            int warnCount = 1;
            foreach (Format00419 warning in miserReplyCFDRI.Formats.OfType<Format00419>().ToArray())
            {

                NDTKI sVTKI = new NDTKI();
                sVTKI.INP_ACTION = "C";
                sVTKI.INP_APPL_CODE = Convert.ToInt32(appCode);
                sVTKI.INP_ACCT_NBR = accNumber;
                sVTKI.INP_NOTE_NBR = warnCount;

                warnCount++;

                string xmlTKI = sVTKI.ToMiser();
                MiserReply mrTKI = new MiserReply(GetSessionHostReply(xmlTKI, _sessionToken));
                if (mrTKI.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";

                }
                else
                {
                    miserReplyCFDRI.Formats.AddRange(mrTKI.Formats.OfType<Format00248>().ToList());
                    ViewBag.repsonse = "Success Results";
                }

            }
            //SessionDisconnect("", _sessionToken);
            //_sessionToken = "";
            return miserReplyCFDRI;
        }
        public ActionResult COOP(string SSN)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            MiserReply miserReplyCFDRI = MiserReply_CFDRI(SSN.ToString(), SessionToken);
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 NO DATA ENTERED")
                || (miserReplyCFDRI.Errors.Contains("ERR000 CIF NOT FOUND"))))
            {
                return new HttpStatusCodeResult(400, "Member Not Found " + SSN);
                //throw new Exception("Member Not Found");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                 && (miserReplyCFDRI.Errors.Contains("ERR000 EMPLOYEE PIN CHG REQUIRED")))
            {
                return new HttpStatusCodeResult(400, "PIN change required to continue.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("Invalid session identifier used. Cannot process transaction.")))
            {
                return new HttpStatusCodeResult(400, "Invalid session identifier used.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 ADDRESS NOT FOUND")))
            {
                return new HttpStatusCodeResult(400, "ADDRESS NOT FOUND.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0)
            {
                throw new Exception(string.Join(" / ", miserReplyCFDRI.Errors));
            }
            List<string> AccountsList =
                miserReplyCFDRI.Formats.OfType<Format00118>().ToList().Where(x => x.RR_MSERS_ACCT_STAT == "OP" &&
                x.RR_MSERS_SERV_ACCT.Length == 16 && x.RR_MSERS_SERV_NBR == 996)
                .Select(x => x.RR_MSERS_SERV_ACCT).Distinct().ToList();
            SoapClient client = new SoapClient();
            client.GetSessionIdentifier("26", "username", "password");
            List<CCdetails> result = client.GetCap(AccountsList);
            SessionDisconnect("", SessionToken);
            _sessionToken = "";
            return PartialView(result);

        }
        public ActionResult GetHiddenBalanceToggle(int sessionID)
        {
            ITBOTDBEntities dc = new ITBOTDBEntities();
            var onBehalfOfList = dc.sbuser_SessionOnBehalfOf.Where(x => x.SessionIdentifier == sessionID).ToList();
            bool onBehalfOf = false;
            if (onBehalfOfList.Count > 0)
                onBehalfOf = onBehalfOfList.First().InBehalfOf;
            return Json(new ActionInfo()
            {
                Success = onBehalfOf,
                SessionID = sessionID
            });
        }

        public ActionResult GetHiddenBalanceToggleLast()
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            int intEmpNum = Convert.ToInt32(EmpNum);
            ITBOTDBEntities dc = new ITBOTDBEntities();

            List<sbuser_TransactionLog> transLst = dc.sbuser_TransactionLog.Where(x => x.TellerNumber == intEmpNum && x.Status != 0 && x.Status != null).ToList();
            int sessionID = transLst.Max(x => x.SessionIdentifier) ?? 0;

            var onBehalfOfList = dc.sbuser_SessionOnBehalfOf.Where(x => x.SessionIdentifier == sessionID).ToList();
            bool onBehalfOf = false;
            if (onBehalfOfList.Count > 0)
                onBehalfOf = onBehalfOfList.First().InBehalfOf;
            return Json(new ActionInfo()
            {
                Success = onBehalfOf,
                SessionID = sessionID
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult HideBalanceToggle(bool isHidden)
        {
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            int intEmpNum = Convert.ToInt32(EmpNum);
            ITBOTDBEntities dc = new ITBOTDBEntities();

            List<sbuser_TransactionLog> transLst = dc.sbuser_TransactionLog.Where(x => x.TellerNumber == intEmpNum && x.Status != 0 && x.Status != null).ToList();
            int? SessionID = transLst.Max(x => x.SessionIdentifier);

            sbuser_SessionOnBehalfOf sobo = new sbuser_SessionOnBehalfOf();
            sobo.SessionIdentifier = SessionID ?? 0;
            sobo.InBehalfOf = isHidden;
            dc.sbuser_SessionOnBehalfOf.AddOrUpdate(sobo);
            dc.SaveChanges();

            return GetHiddenBalanceToggle(SessionID ?? 0);

        }
        public ActionResult SearchByName(string MemberName)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }

            CFDRI cfdri = new CFDRI();
            cfdri.ICF_MM_CUST_TYPE = "P";
            cfdri.ICF_MM_NAME = MemberName;
            cfdri.ICF_DRI_REQ_1 = 124;

            string xml = cfdri.ToMiser();
            MiserReply miserReply = new MiserReply(GetSessionHostReply(xml, _sessionToken));
            if (miserReply.Errors.Count() > 0
                    && (miserReply.Errors.Contains("ERR000 NO DATA ENTERED")
                    || (miserReply.Errors.Contains("ERR000 CUSTOMER NOT FOUND P4205"))))
            {
                return new HttpStatusCodeResult(400, "Member Not Found " + MemberName);
            }
            if (miserReply.Errors.Count() > 0
                && (miserReply.Errors.Contains("Invalid session identifier used. Cannot process transaction.")))
            {
                return new HttpStatusCodeResult(400, "Invalid session identifier used.");
            }
            if (miserReply.Errors.Count() > 0)
            {
                throw new Exception(string.Join(" / ", miserReply.Errors));
            }
            if (miserReply.Status != "A")
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            else
            {
                ViewBag.repsonse = "Success Results";
            }

            SessionDisconnect("", SessionToken);
            SessionToken = "";

            List<MemberAddress> memberAddresses = new List<MemberAddress>();

            foreach (var f119 in miserReply.Formats.OfType<Format00119>())
            {
                MemberAddress ma = new MemberAddress();
                ma.CIF = f119.RR_CFDUP_CIF_NBR.ToString();
                ma.Name = f119.RR_CFDUP_NA_LINE_1;
                ma.StAddress = f119.RR_CFDUP_NA_LINE_2;
                ma.CSZAddress = f119.RR_CFDUP_NA_LINE_3;
                ma.SSN = f119.RR_CFDUP_TAX_ID_NBR.ToString().PadLeft(9,'0');

                memberAddresses.Add(ma);
            }

            foreach (var f112 in miserReply.Formats.OfType<Format00112>())
            {
                MemberAddress ma = new MemberAddress();

                ma.CIF = f112.RR_CFCMI_CIF_NBR.ToString();
                ma.Name = f112.RR_CFCMI_NA_LINE_1;
                ma.StAddress = f112.RR_CFCMI_NA_LINE_2;
                ma.CSZAddress = f112.RR_CFCMI_NA_LINE_3;
                ma.SSN = f112.RR_CFCMI_TAX_ID_NBR.ToString().PadLeft(9, '0'); ;

                memberAddresses.Add(ma);
            }

            return PartialView(memberAddresses);

        }
        public ActionResult searchBySSN_PartialView(string SSN)
         {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }


            MiserReply miserReplyCFDRI = MiserReply_CFDRI(SSN.ToString(), SessionToken);
            //List<MiserReply> mrlist = new List<MiserReply>();
            //mrlist.Add(miserReply);
            List<long> accList =
                miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT !="CL" && x.RR_CFCAR_APPL_CODE !="95" ) 
                .Select(x => x.RR_CFCAR_ACCT_NBR).Distinct().ToList();

            List<long> SVList =
                miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" && x.RR_CFCAR_APPL_CODE == "00")
                .Select(x => x.RR_CFCAR_ACCT_NBR).Distinct().ToList();

            List<long> NDList =
                miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" &&  x.RR_CFCAR_APPL_CODE == "10")
                .Select(x => x.RR_CFCAR_ACCT_NBR).Distinct().ToList();

            foreach (long accNumber in SVList.ToList())
            {
                MiserReply miserReplyCFDRI_ATM = MiserReply_CFDRI_ATM(SSN,"00", accNumber, SessionToken);
                // SVList = miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" && x.RR_CFCAR_APPL_CODE == "00")
                //.Select(x => x.RR_CFCAR_ACCT_NBR).Distinct().ToList();
            }
            foreach (long item in NDList)
            {
            }

            //List<string> accList2 = miserReply.Formats.OfType<Format00216>().ToList()
            //    .Select(x => x.RR_HDR_ACCT_NBR.ToString()).Distinct().ToList();
            //accList.AddRange(accList2);
            List<MiserReply> accDetails = new List<MiserReply>();
            List<MiserReply> MiserReply2 = new List<MiserReply>();
            string strdt=  DateTime.Now.AddMonths(-1).ToString();
            string enDt= DateTime.Now.Date.ToString();
            accDetails = HistoryInquiry(accList, strdt, enDt, SessionToken);
            SessionDisconnect("", SessionToken);
            SessionToken = "";
            return PartialView(accDetails);
        }
        public ActionResult searchBySSN_PartialView_test(string SSN)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }

            MiserReply miserReplyCFDRI = MiserReply_CFDRI(SSN.ToString(), SessionToken);
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 NO DATA ENTERED")
                || (miserReplyCFDRI.Errors.Contains("ERR000 CIF NOT FOUND"))))
            {
                return new HttpStatusCodeResult(400, "Member Not Found " + SSN);
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 EMPLOYEE PIN CHG REQUIRED")))
            {
                return new HttpStatusCodeResult(400, "PIN change required to continue.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("Invalid session identifier used. Cannot process transaction.")))
            {
                return new HttpStatusCodeResult(400, "Invalid session identifier used.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0)
            {
                
                throw new Exception(string.Join(" / ", miserReplyCFDRI.Errors));
                
            }
            //List<MiserReply> mrlist = new List<MiserReply>();
            //mrlist.Add(miserReply);
            string appCode = "";
            List<long> accList =
                miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" && x.RR_CFCAR_APPL_CODE != "95")
                .Select(x => x.RR_CFCAR_ACCT_NBR).Distinct().ToList();
            List<Format00116> f116 = miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" && x.RR_CFCAR_APPL_CODE != "95").ToList();
            List<MiserReply> accDetails = new List<MiserReply>();
            string strdt = DateTime.Now.Date.AddDays(-15).ToString();
            string enDt = DateTime.Now.Date.ToString();

            List<MiserReply> MiserReply2 = new List<MiserReply>();
            accDetails = HistoryInquiry(accList,strdt,enDt, SessionToken);
            List<MemberDetails> memberDetails = new List<MemberDetails>();
            //foreach (Format00116 f in f116)
            //{
            //    format116 mmbrdetls = new format116();
            //    mmbrdetls.AccountBalanceDetails = f.RR_CFCAR_AVL_BAL.ToString();
            //}
                foreach (MiserReply miserReply in accDetails)
            { 
                MemberDetails mmbrdetls = new MemberDetails();
                mmbrdetls.AccNumber = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_ACCT_NBR.ToString();
                mmbrdetls.AppCode = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_APPL_CODE.ToString();
                mmbrdetls.Type = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_ACCT_TYPE.ToString();
                mmbrdetls.MemberName = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_1.ToString();
                mmbrdetls.OtherNames2 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_2 != null ?
                    miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_2.ToString() : null;
                mmbrdetls.OtherNames3 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_3 != null ?
                miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_3.ToString() : null;
                mmbrdetls.OtherNames4 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_4 != null ?
                miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_NAME_4.ToString() : null;

                mmbrdetls.Relationship2 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_2 != null ?
                miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_2.ToString() : null;
                mmbrdetls.Relationship3 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_3 != null ?
                miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_3.ToString() : null;
                mmbrdetls.Relationship4 = miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_4 != null ?
                miserReply.Formats.OfType<MISER.TranGen.Format00216>().FirstOrDefault().RR_HDR_REL_4.ToString() : null;
                if (mmbrdetls.AppCode == "52" || mmbrdetls.AppCode == "45" || mmbrdetls.AppCode == "50" || mmbrdetls.AppCode == "51")
                {
                    mmbrdetls.PrincipalBalance = f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                     .RR_CFCAR_BAL.ToString();
                }
                else {
                    mmbrdetls.Balance = f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                    .RR_CFCAR_BAL.ToString();
                 }
               

                //mmbrdetls.Balance = miserReply.Formats.OfType<MISER.TranGen.Format00243>().Count()
                //    > 0 ? miserReply.Formats.OfType<MISER.TranGen.Format00243>().FirstOrDefault().RR_SVHIS_ACCT_BAL.ToString() : null;

                //mmbrdetls.PrincipalBalance = miserReply.Formats.OfType<MISER.TranGen.Format00315>().Count() 
                //    > 0 ? miserReply.Formats.OfType<MISER.TranGen.Format00315>().FirstOrDefault().RR_LNHIS_PRI_BAL.ToString() : null;
                mmbrdetls.hasATMCard = f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                    .RR_CFCAR_ATM_KI_CODE == null ?"" : f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                    .RR_CFCAR_ATM_KI_CODE.ToString();
                //For CD, ND, SV
                foreach (Format00243 f243 in miserReply.Formats.OfType<MISER.TranGen.Format00243>())
                {
                    AccountHistory acchis = new AccountHistory();
                    acchis.Date = f243.RR_SVHIS_DT.ToString();
                    acchis.TranAmmount = f243.RR_SVHIS_AMT.ToString();
                   // if (acchis.TransactionCode.ToString().Contains("@") || acchis.TransactionCode.ToString().Contains("*") || acchis.TransactionCode.ToString().Contains("FL"))
                        acchis.TransactionCode = f243.RR_SVHIS_TRCD.ToString();
                    acchis.TranAmmount = f243.RR_SVHIS_AMT.ToString();
                    acchis.AccountBalance = f243.RR_SVHIS_ACCT_BAL.ToString();
                    if(mmbrdetls.AppCode == "CD")
                        acchis.MaturityDate = f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                       .RR_CFCAR_MAT_DUE_DT == null ? "" : f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                       .RR_CFCAR_MAT_DUE_DT.ToString();
                    mmbrdetls.AccHistories.Add(acchis);
                }
                //for Loans
                foreach (Format00315 f315 in miserReply.Formats.OfType<MISER.TranGen.Format00315>())
                {
                    AccountHistory acchis = new AccountHistory();
                    acchis.MaturityDate = f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                   .RR_CFCAR_MAT_DUE_DT == null ? "" : f116.Where(x => x.RR_CFCAR_ACCT_NBR.ToString() == mmbrdetls.AccNumber).FirstOrDefault()
                   .RR_CFCAR_MAT_DUE_DT.ToString();
                    acchis.TranAmmount = f315.RR_LNHIS_TOT_PMT.ToString();
                   // if (acchis.TransactionCode.Contains("@") || acchis.TransactionCode.Contains("*") || acchis.TransactionCode.Contains("FL"))
                         acchis.TransactionCode = f315.RR_LNHIS_TRCD.ToString();
                    acchis.PrincipalBalance = f315.RR_LNHIS_PRI_BAL.ToString();
                    acchis.PrincipalBalance = f315.RR_LNHIS_PRI_BAL.ToString();
                    acchis.DueDate = f315.RR_LNHIS_DUEDT.ToString();
                    acchis.PostDate = f315.RR_LNHIS_POST_DT.ToString();
                    mmbrdetls.AccHistories.Add(acchis);
                }
                
                //MiserReply miserReply3 = MiserReply_CFDRI_ATM(SSN, mmbrdetls.AppCode, Convert.ToInt64(mmbrdetls.AccNumber));
                //foreach (MISER.TranGen.Format00168 f in miserReply3.Formats.OfType<MISER.TranGen.Format00168>())
                //{
                //    mmbrdetls.ATMCards.Add(f.RR_MSAAI_PAN);
                //}
                memberDetails.Add(mmbrdetls);
            }
            SessionDisconnect("", SessionToken);
            SessionToken = "";
            return PartialView(memberDetails);
        }

        [HttpPost]//send page details to the controller
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult SSNsearch_Profile_PView()
        {
            return PartialView();
        }
        
        public ActionResult SSNsearch_Profile_PView(string SSN)
        {
            string SessionToken = getToken();
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }

            //List<MiserReply> mrlist = new List<MiserReply>();
            //mrlist.Add(miserReply);
            MiserReply miserReplyCFDRI = MiserReply_CFDRI(SSN.ToString(), SessionToken);
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 NO DATA ENTERED")
                || (miserReplyCFDRI.Errors.Contains("ERR000 CIF NOT FOUND"))))
            {
                return new HttpStatusCodeResult(400, "Member Not Found " + SSN);
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 EMPLOYEE PIN CHG REQUIRED")))
            {
                return new HttpStatusCodeResult(400, "PIN change required to continue.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("Invalid session identifier used. Cannot process transaction.")))
            {
                return new HttpStatusCodeResult(400, "Invalid session identifier used.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0
                && (miserReplyCFDRI.Errors.Contains("ERR000 ADDRESS NOT FOUND")))
            {
                return new HttpStatusCodeResult(400, "ADDRESS NOT FOUND.");
            }
            if (miserReplyCFDRI.Errors.Count() > 0)
            {
                throw new Exception(string.Join(" / ", miserReplyCFDRI.Errors));
            }
            List<string> otherNamesList =
                miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" && x.RR_CFCAR_NAME_1 != null)
                .Select(x => x.RR_CFCAR_NAME_1).Distinct().ToList();
            
            List<MiserReply> listMiserReply = new List<MiserReply>();
            listMiserReply.Add(miserReplyCFDRI);//adds only one MiserReply to the list
            List<Format00116> accountsList = miserReplyCFDRI.Formats.OfType<Format00116>().ToList().Where(x => x.RR_CFCAR_ACCT_STAT != "CL" ).ToList();

            var model = new SSNsearch_Profile_PView()
            {

                // You'll likely want a .ToList() after these to ensure things work as expected
                MiserReply = listMiserReply,
                otherNames = otherNamesList,
            };
            //return PartialView(listMiserReply);

            model.wFlags = new Dictionary<string, List<string>>();
            model.lFlags = new Dictionary<string, List<string>>();
            model.OtherNamesNSsn = new Dictionary<string, List<OtherNamesNssn>>();
            model.oFlags = new Dictionary<string, List<string>>();
            model.hasATM = new Dictionary<string, string>();
            foreach (var acc in accountsList)
            {
                MiserReply mr = 
                MiserReply_CFDRI_Flag(SSN.ToString(), acc.RR_CFCAR_APPL_CODE, acc.RR_CFCAR_ACCT_NBR, SessionToken);
                List<string> wfDesc = new List<string>();
                List<string> lfDesc = new List<string>();
                List<string> ofDesc = new List<string>();
                foreach (var f419 in mr.Formats.OfType<Format00419>())//Format00419
                {
                    if (!f419.RR_WARN_FLAG_DESC.Contains("TICKLER"))
                        wfDesc.Add(f419.RR_WARN_FLAG_DESC);
                }
                foreach (var f248 in mr.Formats.OfType<Format00248>())//Format00419
                {
                    if (f248.RR_NPAL2_LINE_INFO != "NO MORE LINES PRESENT")
                        wfDesc.Add(f248.RR_NPAL2_LINE_INFO);
                }
                foreach (var f418 in mr.Formats.OfType<Format00418>())
                {
                    lfDesc.Add(f418.RR_LOCK_FLAG_DESC);
                }
                foreach (var f420 in mr.Formats.OfType<Format00420>())
                {
                    ofDesc.Add(f420.RR_OTHER_FLAG_DESC);
                }
                List<OtherNamesNssn> onLst = new List<OtherNamesNssn>();
                foreach (var f216 in mr.Formats.OfType<Format00216>())
                {
                    OtherNamesNssn onns1 = new OtherNamesNssn();
                    onns1.Name = f216.RR_HDR_NAME_1;
                    onns1.SSN = f216.RR_HDR_TAX_ID_1.ToString();

                    OtherNamesNssn onns2 = new OtherNamesNssn();
                    onns2.Name = f216.RR_HDR_NAME_2;
                    onns2.SSN = f216.RR_HDR_TAX_ID_2.ToString();

                    OtherNamesNssn onns3 = new OtherNamesNssn();
                    onns3.Name = f216.RR_HDR_NAME_3;
                    onns3.SSN = f216.RR_HDR_TAX_ID_3.ToString();

                    OtherNamesNssn onns4 = new OtherNamesNssn();
                    onns4.Name = f216.RR_HDR_NAME_4;
                    onns4.SSN = f216.RR_HDR_TAX_ID_4.ToString();

                    onLst.Add(onns1);
                    onLst.Add(onns2);
                    onLst.Add(onns3);
                    onLst.Add(onns4);

                }
                model.hasATM[acc.RR_CFCAR_ACCT_NBR.ToString()] =
                        string.IsNullOrWhiteSpace(acc.RR_CFCAR_ATM_KI_CODE) ? "" : acc.RR_CFCAR_ATM_KI_CODE;
                model.OtherNamesNSsn[acc.RR_CFCAR_ACCT_NBR.ToString()] = onLst;
                model.wFlags[acc.RR_CFCAR_ACCT_NBR.ToString()] = wfDesc;
                model.lFlags[acc.RR_CFCAR_ACCT_NBR.ToString()] = lfDesc;
                model.oFlags[acc.RR_CFCAR_ACCT_NBR.ToString()] = ofDesc;
            }

            SessionDisconnect("", SessionToken);
            SessionToken = "";
            return PartialView(model);
        }

        public List<MiserReply> HistoryInquiry(List<long> accList, string strDT, string enDt, string SessionToken)
        {
            if (string.IsNullOrEmpty(SessionToken))
            {
                ViewBag.repsonse = "Unsuccessful result";
            }
            List<MiserReply> accDetails = new List<MiserReply>();
            //ND His
            foreach (long accNumber in accList)

            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                NDHIS nDHIS = new NDHIS();
                nDHIS.IND_ACCT_NBR = Convert.ToInt64(accNumber);
                //var result = DateTime.ParseExact(dateString, dd/MM/yyyy HH:mm", new CultureInfo("en-US"));
                nDHIS.IND_DATE_1 = Convert.ToDateTime(strDT);
               // nDHIS.IND_DATE_1 = strDT;
                nDHIS.IND_DATE_2 = Convert.ToDateTime(enDt);
                string xmlND = nDHIS.ToMiser();
                MiserReply miserReplyND = new MiserReply(GetSessionHostReply(xmlND, _sessionToken));
                if (miserReplyND.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    accDetails.Add(miserReplyND);
                    ViewBag.repsonse = "Success Results";
                }
                //NDBAL nDBAL = new NDBAL();
                //// nDBAL.IND_ACCT_NBR = Convert.ToInt64(accNumber);
                //nDBAL.IND_ACCT_NBR = accNumber;
                //string xmlNDBal = nDBAL.ToMiser();
                //MiserReply miserReplyNDBal = new MiserReply(GetSessionHostReply(xmlNDBal, _sessionToken));
                //if (miserReplyNDBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyNDBal);
                //    ViewBag.repsonse = "Success Results";
                //}
            }

            //NL His
            foreach (long accNumber in accList)

            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                NLHIS nLHIS = new NLHIS();
                nLHIS.INL_ACCT_NBR = Convert.ToInt64(accNumber);
                nLHIS.INL_IQ_FROM_DATE = Convert.ToDateTime(strDT);
                nLHIS.INL_IQ_TO_DATE = Convert.ToDateTime(enDt);
                string xmlNL = nLHIS.ToMiser();
                MiserReply miserReplyNL = new MiserReply(GetSessionHostReply(xmlNL, _sessionToken));
                if (miserReplyNL.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    accDetails.Add(miserReplyNL);
                    ViewBag.repsonse = "Success Results";
                }
                //NLBAL nLBAL = new NLBAL();
                //nLBAL.INL_ACCT_NBR = accNumber;
                //string xmlNLBal = nLBAL.ToMiser();
                //MiserReply miserReplyNLBal = new MiserReply(GetSessionHostReply(xmlNLBal, _sessionToken));
                //if (miserReplyNLBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyNLBal);
                //    ViewBag.repsonse = "Success Results";
                //}
            }
            //ML His
            foreach (long accNumber in accList)

            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                MLHIS mLHIS = new MLHIS();
                mLHIS.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                mLHIS.IL_HIS_FROM_DATE = Convert.ToDateTime(strDT);
                mLHIS.IL_HIS_THRU_DATE = Convert.ToDateTime(enDt);
                string xmlmL = mLHIS.ToMiser();
                MiserReply miserReplyML = new MiserReply(GetSessionHostReply(xmlmL, _sessionToken));
                if (miserReplyML.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    accDetails.Add(miserReplyML);
                    ViewBag.repsonse = "Success Results";
                }
                //MLBAL mLBAL = new MLBAL();
                //mLBAL.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                //string xmlMLBal = mLBAL.ToMiser();
                //MiserReply miserReplyMLBal = new MiserReply(GetSessionHostReply(xmlMLBal, _sessionToken));
                //if (miserReplyMLBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyMLBal);
                //    ViewBag.repsonse = "Success Results";
                //}
            }

            //SV His
            foreach (long accNumber in accList)
            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                SVHIS sVHIS = new SVHIS();
                //SV history needs reason code in orderfor the date range to take effect
                sVHIS.Headers.ReasonCode = "1";
                sVHIS.IS_ACCT_NBR = Convert.ToInt64(accNumber);
                sVHIS.IS_DATE_1 = Convert.ToDateTime(strDT);
                sVHIS.IS_DATE_2 = Convert.ToDateTime(enDt);
                
                string xmlSV = sVHIS.ToMiser();

                MiserReply miserReplySV = new MiserReply(GetSessionHostReply(xmlSV, _sessionToken));
                //miserReplySV = miserReplySV.Where(x => !x.RR_SVHIS_TRCD.Contains("@") && !x.RR_SVHIS_TRCD.Contains("*")
                    //    && x.RR_SVHIS_POST_DT > Convert.ToDateTime(strDT) && x.RR_SVHIS_POST_DT < Convert.ToDateTime(enDt)));
                if (miserReplySV.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {//The commented code was added when the date range was not working and removed once the header for reason code was added
                    //List<IMiserReplyBase> allReplies = miserReplySV.Formats.ToArray().ToList();

                    //miserReplySV.Formats = allReplies.OfType<Format00243>()
                    //.Where(x => x.RR_SVHIS_DT >= Convert.ToDateTime(strDT)
                    //&& x.RR_SVHIS_DT <= Convert.ToDateTime(enDt))
                    //.ToList().OfType<IMiserReplyBase>().ToList();

                    //miserReplySV.Formats.AddRange(allReplies.OfType<Format00098>().ToList().OfType<IMiserReplyBase>().ToList());//IMiserReplyBase formato general de miser replies
                    //miserReplySV.Formats.AddRange(allReplies.OfType<Format00100>().ToList().OfType<IMiserReplyBase>().ToList());
                    //miserReplySV.Formats.AddRange(allReplies.OfType<Format00216>().ToList().OfType<IMiserReplyBase>().ToList());
                    accDetails.Add(miserReplySV);
                    //accDetails = accDetails.Where(x => x.Formats.OfType<Format00243>().ToList())
                         //NDdetails.AddRange(miserReplyND.Formats.OfType<Format00243>().ToList());
                    ViewBag.repsonse = "Success Results";
                }
                //SVBAL sVBAL = new SVBAL();
                //sVBAL.IS_ACCT_NBR = Convert.ToInt64(accNumber);
                //string xmlMSVBal = sVBAL.ToMiser();
                //MiserReply miserReplySVBal = new MiserReply(GetSessionHostReply(xmlMSVBal, _sessionToken));
                //if (miserReplySVBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplySVBal);
                //    ViewBag.repsonse = "Success Results";
                //}

            }
            //CD His
            foreach (long accNumber in accList)
            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                CDHIS cDHIS = new CDHIS();
                cDHIS.IS_ACCT_NBR = Convert.ToInt64(accNumber);
                cDHIS.IS_DATE_1 = Convert.ToDateTime(strDT);
                cDHIS.IS_DATE_1 = Convert.ToDateTime(strDT);
                string xmlCD = cDHIS.ToMiser();

                MiserReply miserReplyCD = new MiserReply(GetSessionHostReply(xmlCD, _sessionToken));
                if (miserReplyCD.Status != "A")
                {
                    ViewBag.repsonse = "Unsuccessful result";
                }
                else
                {
                    List<IMiserReplyBase> allReplies = miserReplyCD.Formats.ToArray().ToList();

                    miserReplyCD.Formats = allReplies.OfType<Format00243>()
                    .Where(x => x.RR_SVHIS_DT >= Convert.ToDateTime(strDT)
                    && x.RR_SVHIS_DT <= Convert.ToDateTime(enDt))
                    .ToList().OfType<IMiserReplyBase>().ToList();

                    miserReplyCD.Formats.AddRange(allReplies.OfType<Format00098>().ToList().OfType<IMiserReplyBase>().ToList());//IMiserReplyBase formato general de miser replies
                    miserReplyCD.Formats.AddRange(allReplies.OfType<Format00100>().ToList().OfType<IMiserReplyBase>().ToList());
                    miserReplyCD.Formats.AddRange(allReplies.OfType<Format00216>().ToList().OfType<IMiserReplyBase>().ToList());
                    accDetails.Add(miserReplyCD);
                    ViewBag.repsonse = "Success Results";
                }
                //CDBAL cDBAL = new CDBAL();
                //cDBAL.IS_ACCT_NBR = Convert.ToInt64(accNumber);
                //string xmlMCDBal = cDBAL.ToMiser();
                //MiserReply miserReplyCDBal = new MiserReply(GetSessionHostReply(xmlMCDBal, _sessionToken));
                //if (miserReplyCDBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyCDBal);
                //    ViewBag.repsonse = "Success Results";
                //}

            }
            //OL Hisaaaaaaaaaaaaaaa
            foreach (long accNumber in accList)
            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                OLHIS oLHIS = new OLHIS();
                oLHIS.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                oLHIS.IL_HIS_FROM_DATE = Convert.ToDateTime(strDT);
                oLHIS.IL_HIS_FROM_DATE = Convert.ToDateTime(strDT);
                string xmlOL = oLHIS.ToMiser();
                MiserReply miserReplyOL = new MiserReply(GetSessionHostReply(xmlOL, _sessionToken));
                if (miserReplyOL.Status != "A") { ViewBag.repsonse = "Unsuccessful result"; }
                else
                {
                    accDetails.Add(miserReplyOL);
                    ViewBag.repsonse = "Success Results";
                }
                //OLBAL oLBAL = new OLBAL();
                //oLBAL.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                //string xmlOLBal = oLBAL.ToMiser();
                //MiserReply miserReplyOLBal = new MiserReply(GetSessionHostReply(xmlOLBal, _sessionToken));
                //if (miserReplyOLBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyOLBal);
                //    ViewBag.repsonse = "Success Results";
                //}
            }
            //IL History
            foreach (long accNumber in accList)
            {
                if (string.IsNullOrWhiteSpace(accNumber.ToString()))
                    continue;
                ILHIS iLHIS = new ILHIS();
                iLHIS.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                iLHIS.IL_HIS_FROM_DATE = Convert.ToDateTime(strDT);
                iLHIS.IL_HIS_FROM_DATE = Convert.ToDateTime(strDT);
                string xmlIL = iLHIS.ToMiser();
                MiserReply miserReplyIL = new MiserReply(GetSessionHostReply(xmlIL, _sessionToken));
                if (miserReplyIL.Status != "A") { ViewBag.repsonse = "Unsuccessful result"; }
                else
                {
                    accDetails.Add(miserReplyIL);
                    ViewBag.repsonse = "Success Results";
                }
                //ILBAL iLBAL = new ILBAL();
                //iLBAL.IL_ACCT_NBR = Convert.ToInt64(accNumber);
                //string xmlILBal = iLBAL.ToMiser();
                //MiserReply miserReplyILBal = new MiserReply(GetSessionHostReply(xmlILBal, _sessionToken));
                //if (miserReplyILBal.Status != "A")
                //{
                //    ViewBag.repsonse = "Unsuccessful result";
                //}
                //else
                //{
                //    MiserReply2.Add(miserReplyILBal);
                //    ViewBag.repsonse = "Success Results";
                //}
            }
            
            //ndAndSVdetails.SVdetails = SVdetails;
            //ndAndSVdetails.NDdetails = NDdetails;
            return accDetails;
        }

        public ActionResult FlagsDescPopUp(string flagDesc)
        {
            List<string> flagLst = flagDesc.Split(';').ToList();
            return PartialView(flagLst);
        }
        public ActionResult cfTinPartial()
        {
            ViewBag.repsonse = "Not run";
            return PartialView();
        }
       
        [HttpPost]//send page details to the controller
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public ActionResult cfTinPartial(MiserLoginModel LoginDetails)
        {
            return PartialView();
        }

        public ActionResult LogOutDB()//method for delete (log out) view
        {
            try
            {
                string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
                ViewBag.identityName = EmpNum;
                ITBOTDBEntities dataContext = new ITBOTDBEntities();//to delete
                dataContext.sbuser_ncr_miser.RemoveRange(dataContext.sbuser_ncr_miser.Where(x => x.Teller_Num == EmpNum));
                dataContext.SaveChanges();//to send to the DB
                return RedirectToAction("MiserLoginInfo");
            }
            catch (Exception ex)
            { 
                throw;
            }
        }
        public ActionResult LogOut()//method for delete (log out) view
        {
             return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult MiserLoginInfo(MiserLoginModel LoginDetails )
        {
            string response = CallWebService(LoginDetails);
            ITBOTDBEntities dataContext = new ITBOTDBEntities();
            //string response = "ERR000 EMP FROZEN PIN VIOLATION";
            if (response.Contains("OK")  )
            {
                
                    sbuser_ncr_miser MiserDetails = new sbuser_ncr_miser();
                    MiserDetails.Miser_STA = LoginDetails.MiserStation;
                    MiserDetails.Teller_Num = LoginDetails.TellerNumber;
                    MiserDetails.Pin = LoginDetails.Password;
                    MiserDetails.Cash_Box_Num = LoginDetails.CashBoxNum;
                    MiserDetails.Beginning_Cash = LoginDetails.BegginingCash;
                    MiserDetails.Daily_Var_Date = LoginDetails.DailyVarDate;
                    //ITBOTDBEntities dataContext = new ITBOTDBEntities();
                    dataContext.sbuser_ncr_miser.Add(MiserDetails);
                    dataContext.SaveChanges();//to send to the DB
                    return RedirectToAction("LoggedIn");
                   
            }
            else
            {
                TempData["PostCalled"] = true;
                TempData["color"] = "red";
                if (response.Contains("ERR805 PIN OR OFFSET ERROR"))
                {
                    //ViewBag.Errors = new string[] { "You have entered a wrong Pin", "Please try again, only 3 tries accpeted until your accouns is locked." };
                    TempData["Message"] = string.Format("You have entered a wrong Pin");
                    TempData["Message2"] = "Please try again, only 3 tries allowed before your accouns gets locked.";

                }
                if (response.Contains("ERR000 EMP FROZEN PIN VIOLATION"))
                {
                    TempData["Message"] = string.Format("Unsuccessful authentication for {0}, ERR000 EMP FROZEN PIN VIOLATION. ", LoginDetails.TellerNumber).Replace("\r\n", "<br/>").Replace("\r\n", "<br/>");
                    TempData["Message2"] = "Your account has been locked, please click <a href=\"http://test.com\">MISER PIN reset</a>. to unlock your account.";

                }
                TempData["Message"] = response.ToString().Replace("\r\n", "<br/>").Replace("\r\n", "<br/>");

              
                //ViewBag.Message = string.Format("Hello {0}.\\nCurrent Date and Time: {1} ",LoginDetails.TellerNumber,  DateTime.Now.ToString());
            }
            return RedirectToAction("MiserLoginInfo");//View();If view is used it will submit the form again if is refreshed
        }
        public ActionResult MiserLoginInfo()
        {
            ITBOTDBEntities dataContext = new ITBOTDBEntities();
            string pcName = System.Net.Dns.GetHostEntry(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_HOST"]).HostName;
            pcName = pcName.Replace(".username.org", "");
            string MiserSTA = dataContext.sbuser_PCNameMiserStation.Where(x => x.PCName == pcName).Count()<= 0 ? pcName: 
                dataContext.sbuser_PCNameMiserStation.Where(x => x.PCName == pcName).FirstOrDefault().MiserStation.ToString();
            TempData["HostName"] = MiserSTA;
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            string UserID = User.Identity.Name;
            ViewBag.identityName = UserID;
            TempData["UserID"] = UserID;
            ViewBag.identityName = EmpNum;
            TempData["EmpName"] = EmpNum;
            if (TempData["PostCalled"] == null)
            {
                TempData["color"] = null;
                TempData["Message"] = null;
                TempData["Message2"] = null;
            }
            TempData["PostCalled"] = null;
            TempData.Keep();
            return View();
        }

        public ActionResult AddMiserSTA()
        {
            ITBOTDBEntities dc = new ITBOTDBEntities();
            List<sbuser_MemberProfileAdmins> adminsList = dc.sbuser_MemberProfileAdmins.ToList();
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            string EmpName = User.Identity.Name;
            TempData["UserID"] = EmpName;
            TempData.Keep();
            int EmpNumInt = Convert.ToInt32(EmpNum);
            if (!adminsList.Select(x => x.UserName).Contains(EmpNumInt))
            {
                return RedirectToAction("LoggedIn");
            }
                return View();
            
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddMiserSTA(sbuser_PCNameMiserStation pcNameMiserSTA)
        {
            ITBOTDBEntities dc = new ITBOTDBEntities();
            List<sbuser_MemberProfileAdmins> adminsList = dc.sbuser_MemberProfileAdmins.ToList();
            string EmpNum = Regex.Replace(User.Identity.Name, @"[^\d]", "");
            int EmpNumInt = Convert.ToInt32(EmpNum);
            if (!adminsList.Select(x => x.UserName).Contains(EmpNumInt))
            {
                return RedirectToAction("LoggedIn");
            }
            dc.sbuser_PCNameMiserStation.Add(pcNameMiserSTA);
            dc.SaveChanges();
            return RedirectToAction("AddMiserSTA");
        }

        /// <summary>
        /// Process a transaction  
        /// Will call web service to preform a SV DP+
        /// </summary>
        /// <param name="trans">Transaction information</param>
        /// <param name="pipe">Database context to writ message to the screen</param>
        private static string CallWebService(MiserLoginModel trans)
        {
            try
            {
                string jsonInput =
                    "{\"StationName\":\"" + trans.MiserStation + "\", "
                    + "\"PIN\":\"" + trans.Password + "\", "
                    + "\"TellerNumber\":\"" + trans.TellerNumber + "\", "
                    + "\"CashBox\":\"" + trans.CashBoxNum + "\", "
                    + "\"DailyVarDate\":\"" + trans.DailyVarDate + "\", "
                    + "\"inBeginningCash\":\"" + trans.BegginingCash + "\""
                    + "}";
               // jsonInput = CLRTestSQLCohesionServer.AesOperation.EncryptString(Key, jsonInput);
               
                string HTMLRes = GetHTMLResult(jsonInput);

                return HTMLRes;
            }
            catch (Exception ex)
            {
                return ex.Message;
               // pipe.Send("Error: " + ex.Message);
            }
        }
       
        

        private static List<Format00243> CFTIN(string EmpNum, string AccNum)
        {
            try
            {
                UserQueryInfo queryInfo = new UserQueryInfo();
                queryInfo.TransactionName = "cftin";
                queryInfo.UserId = EmpNum;
                queryInfo.TransactionJson =
                    "{\"IND_ACCT_NBR\":\"" + AccNum + "\""
                    + "}";
                //jsonInput = CLRTestSQLCohesionServer.AesOperation.EncryptString(Key, jsonInput);
                string jsonInput = JsonConvert.SerializeObject(queryInfo);
                string HTMLRes = GetUserQueryResult(jsonInput);
                return JsonConvert.DeserializeObject<List<Format00243>>(HTMLRes);
            }
            catch (Exception ex)
            {
                return new List<Format00243>();
                // return   "";
                // pipe.Send("Error: " + ex.Message);
            }
        }
        private static string GetHTMLResult(string jsonInput)
        {
            string uri = ConfigurationManager.AppSettings["MscurAPI"];
            string parameters = "=" + jsonInput;
            string HtmlResult = "";
            using (WebClient client = new WebClient())
            {
                var reqparm = new System.Collections.Specialized.NameValueCollection();
                reqparm.Add("", jsonInput);
                byte[] responsebytes = client.UploadValues(uri, "POST", reqparm);
                string responsebody = Encoding.UTF8.GetString(responsebytes);
                HtmlResult = responsebody;
            }
            return HtmlResult;
        }

        private static string GetUserQueryResult(string jsonInput)
        {
            string uri = ConfigurationManager.AppSettings["UserQueryAPI"];
            string parameters = "=" + jsonInput;
            string HtmlResult = "";
            using (WebClient client = new WebClient())
            {
                var reqparm = new System.Collections.Specialized.NameValueCollection();
                reqparm.Add("", jsonInput);
                byte[] responsebytes = client.UploadValues(uri, "POST", reqparm);
                string responsebody = Encoding.UTF8.GetString(responsebytes);

                HtmlResult = responsebody;
            }
            return HtmlResult;
        }
        /// <summary>
        /// gets the reply from miser
        /// </summary>
        /// <param name="tran">input transaction on xml format</param>
        /// <returns></returns>
        private static string GetHostReply(string tran, string username)
        {
            try
            {
                using (CohesionConnectClient ccc = new CohesionConnectClient())
                {


                    using (OperationContextScope scope = new OperationContextScope(ccc.InnerChannel))
                    {

                        MessageHeader hdrUserName = MessageHeader.CreateHeader(
                            "UserName", SVC_NAMESPACE, username);

                        MessageHeader hdrToken = MessageHeader.CreateHeader(
                            "AuthToken", SVC_NAMESPACE, _sessionToken);

                        OperationContext.Current.OutgoingMessageHeaders.Add(hdrUserName);
                        OperationContext.Current.OutgoingMessageHeaders.Add(hdrToken);
                        return ccc.GetHostReply(tran);
                    }

                    //return ccc.GetHostReply(tran);
                }
            }
            catch (Exception ex)
            {
                //log.Fatal(ex.Message);
                return "";
            }
        }//GetHostReply()


        /// <summary>
        /// Encrypts the password using RSA encryption and the specified public key.
        /// </summary>
        /// <param name="pwd">Password to encrypt</param>
        /// <param name="publicKey">RSA public key used for encryption</param>
        /// <returns>Base64 encoded encrypted password</returns>
        private static string GetEncryptedPassword(string pwd, string publicKey)
        {
            byte[] pubKeyBytes = Convert.FromBase64String(publicKey);
            byte[] expBytes = { 1, 0, 1 }; // Hardcoded value used by the .net framework.
            string retVal = null;
            //Create a new instance of RSACryptoServiceProvider.
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
            {
                //Create a new instance of RSAParameters.
                RSAParameters rsaKeyInfo = new RSAParameters();

                //Set RSAKeyInfo to the public key values. 
                rsaKeyInfo.Modulus = pubKeyBytes;
                rsaKeyInfo.Exponent = expBytes;

                // Import key parameters into RSA.
                rsa.ImportParameters(rsaKeyInfo);

                byte[] encData = rsa.Encrypt(Encoding.Default.GetBytes(pwd), false);
                retVal = Convert.ToBase64String(encData);
            }
            return retVal;
        }
        /// <summary>
        /// disconnect session. 
        /// Needed to connect teller from another computer.
        /// </summary>
        /// <param name="returnRes">Log text</param>
        /// <returns>log text</returns>
        private static string SessionDisconnect(string returnRes, string sessionToken)
        {
            using (CohesionConnectClient ccc = new CohesionConnectClient())
            {

                ccc.SessionDisconnect(sessionToken, true);
                sessionToken = "";

                returnRes += "Disconnect from Session completed." + System.Environment.NewLine;
            }

            returnRes += "Session Token: " + sessionToken + Environment.NewLine;

            return returnRes;
        }


        /// <summary>
        /// gets the reply from miser
        /// </summary>
        /// <param name="tran">input transaction on xml format</param>
        /// <returns></returns>
        private static string GetSessionHostReply(string tran, string sessionToken)
        {
            try
            {
                using (CohesionConnectClient ccc = new CohesionConnectClient())
                {


                    using (OperationContextScope scope = new OperationContextScope(ccc.InnerChannel))
                    {

                        //MessageHeader hdrUserName = MessageHeader.CreateHeader(
                        //    "UserName", SVC_NAMESPACE, username);

                        //MessageHeader hdrToken = MessageHeader.CreateHeader(
                        //    "AuthToken", SVC_NAMESPACE, _sessionToken);

                        //OperationContext.Current.OutgoingMessageHeaders.Add(hdrUserName);
                        //OperationContext.Current.OutgoingMessageHeaders.Add(hdrToken);
                        return ccc.SessionGetHostReply(sessionToken, tran);
                    }

                    //return ccc.GetHostReply(tran);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }//GetSessionHostReply()

        /// <summary>
        /// Validates entered credentials against the Cohesion server and returns a token if validation succeeds. 
        /// Throws an application exception if validation fails.
        /// </summary>
        /// <param name="user">User name</param>
        /// <param name="pwd">Password</param>
        /// <returns>Session token</returns>
        private static string WcfAuthenticate(string user, string pwd)
        {
            string sessionToken = null;

            CohesionWCF.PreAuthInfo pai;
            using (CohesionWCF.CohesionConnectClient cc = new CohesionWCF.CohesionConnectClient("endpointHttp_ICohesionConnect"))
            {
                pai = cc.AuthenticateStep1(user);
            }

            CohesionWCF.LogonResult authResult;
            using (CohesionWCF.CohesionConnectClient cc = new CohesionWCF.CohesionConnectClient("endpointHttp_ICohesionConnect"))
            {
                authResult = cc.AuthenticateStep2(
                    pai.UserToken,
                    GetEncryptedPassword(pwd, pai.PublicKey));
            }

            if (authResult.Result)
            {
                sessionToken = authResult.Token;
            }
            else
            {
                throw new ApplicationException(string.Format("Authentication error. {0}", authResult.Message));
            }

            return sessionToken;
        }

    }

}