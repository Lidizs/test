﻿using MVCMiserLogin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCMiserLogin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult TransMonitor()
        {
            return PartialView();
        }

        public ActionResult SearchByProfile(string SSN)
        {
            return PartialView();
        }

        #region Ajax Methods
        public ActionResult GetDataTransMonitor()
        {
            List<MonitorPartialModel> model = new List<MonitorPartialModel>() {
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943279",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943280",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943279",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943281",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },
                new MonitorPartialModel{
                    BranchID = "Mesa Hills",
                    SourceAccountNumber = "8062006",
                    SourceAccountType="Checking",
                    DestinationAccountNumber = "75738521",
                    DestinationAccountType = "ML regular payment",
                    TransactionAmount="50.00",
                    Memo = string.Empty,
                    SessionIdentifier = "14231344",
                    TransactionIdentifier = "10943282",
                    TransactionType = "Transfer",
                    Status = new Random().Next(1,2).ToString(),
                    CreationDate = DateTime.Now,
                    MachineGroupName = "MachineGroup"
                },

            };

            return Json(model);

        } 
        #endregion
    }
}