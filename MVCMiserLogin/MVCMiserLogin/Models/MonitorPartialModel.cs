﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCMiserLogin.Models
{
    public class MonitorPartialModel
    {
        public string BranchID { get; set; }
        public string SourceAccountNumber { get; set; }
        public string DestinationAccountNumber { get; set; }
        public string SourceAccountType { get; set; }
        public string DestinationAccountType { get; set; }
        public string TransactionAmount { get; set; }
        public string Memo { get; set; }
        public string SessionIdentifier { get; set; }
        public string MachineGroupName { get; set; }
        public string Status { get; set; }
        public string TransactionIdentifier { get; set; }
        public string TransactionType { get; set; }
        public DateTime CreationDate { get; set; }
    }
}