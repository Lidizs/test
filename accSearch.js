$(document).ready(function () {
    $(function () {
        $('#searchByAccNumber').on('click', function (evt) {
            evt.preventDefault();
            $.ajax({
                url: "SearchByAccNumber",
                //url: "SNNsearch_Profile_PView",
                type: "get",
                data: {
                    "accNum2": $("input[name='accNum2']").val().replace(/\-/g, "")
                },
                success: function (result) {
                    $("#searchByAccNumberPartial").html = "";
                    $("#searchByAccNumberPartial").html(result);
                    //initializeAmountProcessed();
                }
            });
        });
    }); 
});

   